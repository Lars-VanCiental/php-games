<?php

namespace LVC\PHPGames\Domain\Player;

use LVC\PHPGames\Domain\Gameplay\Command;

interface PlayerInterface
{
    /** @return \Generator|Command[] */
    public function play(): \Generator;
}
