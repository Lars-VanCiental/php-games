<?php

namespace LVC\PHPGames\Domain\Level\Grid;

class Coordinates
{
    const COORDINATES_SEPARATOR = ',';

    /** @var int */
    private $x;
    /** @var int */
    private $y;

    public function __construct(int $x, int $y)
    {
        $this->x = $x;
        $this->y = $y;
    }

    public function getX(): int
    {
        return $this->x;
    }

    public function getY(): int
    {
        return $this->y;
    }

    public function checkWithin(int $width, int $height): bool
    {
        return !($this->x < 0 || $this->x >= $width || $this->y < 0 || $this->y >= $height);
    }

    public function __toString(): string
    {
        return $this->x.self::COORDINATES_SEPARATOR.$this->y;
    }
}
