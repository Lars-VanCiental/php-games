<?php

namespace LVC\PHPGames\Domain\Level\Grid;

trait GridLevel
{
    /** @var int */
    private $width;
    /** @var int */
    private $height;

    public function getWidth(): int
    {
        return $this->width;
    }

    public function getHeight(): int
    {
        return $this->height;
    }
}
