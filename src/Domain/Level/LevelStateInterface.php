<?php

namespace LVC\PHPGames\Domain\Level;

interface LevelStateInterface
{
    public function getLevel(): LevelInterface;

    public function getState(): string;

    public function isVictory(): bool;
}
