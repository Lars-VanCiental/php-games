<?php

namespace LVC\PHPGames\Domain\Level;

interface LevelInterface
{
    public function start(): LevelStateInterface;
}
