<?php

namespace LVC\PHPGames\Domain\Level;

interface LevelFactoryInterface
{
    /** @throws InvalidLevelException */
    public function loadLevel(string $levelName): LevelInterface;
}
