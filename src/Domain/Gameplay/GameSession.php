<?php

namespace LVC\PHPGames\Domain\Gameplay;

use LVC\PHPGames\Domain\Level\LevelInterface;
use LVC\PHPGames\Domain\Level\LevelStateInterface;
use LVC\PHPGames\Domain\Player\PlayerInterface;

class GameSession
{
    /** @var RulesInterface */
    private $rules;
    /** @var LevelInterface */
    private $level;
    /** @var PlayerInterface */
    private $player;

    public function __construct(RulesInterface $rules, LevelInterface $level, PlayerInterface $player)
    {
        $this->rules = $rules;
        $this->level = $level;
        $this->player = $player;
    }

    /** @return \Generator|LevelStateInterface[] */
    public function play(): \Generator
    {
        yield $initialState = $this->level->start();

        $levelStateGenerator = $this->rules->play($initialState);
        $playerCommandsGenerator = $this->player->play();

        while ($levelStateGenerator->valid() && $playerCommandsGenerator->valid()) {
            $levelState = $levelStateGenerator->send($playerCommandsGenerator->current());
            if ($levelState instanceof LevelStateInterface) {
                yield $levelState;
            }

            if (!$levelStateGenerator->valid()) {
                break;
            }

            $playerCommandsGenerator->next();
        }

        if (!$levelStateGenerator->valid() && $finalLevelState = $levelStateGenerator->getReturn() instanceof LevelStateInterface) {
            yield $levelStateGenerator->getReturn();
        }
    }
}
