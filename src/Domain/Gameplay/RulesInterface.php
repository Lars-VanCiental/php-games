<?php

namespace LVC\PHPGames\Domain\Gameplay;

use LVC\PHPGames\Domain\Level\LevelStateInterface;

interface RulesInterface
{
    /** @return Command[] */
    public function getAvailableCommands(): array;

    /**
     * must generate LevelStateInterface while accepting commands from the generator function send
     * when the game is over, it must return a final level state
     *
     * @param LevelStateInterface $initialLevelState
     *
     * @return \Generator|LevelStateInterface[]
     */
    public function play(LevelStateInterface $initialLevelState): \Generator;
}
