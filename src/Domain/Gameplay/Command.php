<?php

namespace LVC\PHPGames\Domain\Gameplay;

class Command
{
    /** @var string */
    private $command;

    public function __construct(string $command)
    {
        $this->command = $command;
    }

    public function getCommand(): string
    {
        return $this->command;
    }
}
