<?php

namespace LVC\PHPGames\Domain\Gameplay;

use LVC\PHPGames\Domain\Level\LevelFactoryInterface;
use LVC\PHPGames\Domain\Player\PlayerInterface;

class Game
{
    /** @var RulesInterface */
    private $rules;
    /** @var LevelFactoryInterface */
    private $levelFactory;

    public function __construct(RulesInterface $rules, LevelFactoryInterface $levelFactory)
    {
        $this->rules = $rules;
        $this->levelFactory = $levelFactory;
    }

    public function startSession(PlayerInterface $player, string $levelName): GameSession
    {
        return new GameSession($this->rules, $this->levelFactory->loadLevel($levelName), $player);
    }
}
