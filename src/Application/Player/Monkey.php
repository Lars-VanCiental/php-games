<?php

namespace LVC\PHPGames\Application\Player;

use LVC\PHPGames\Domain\Gameplay\Command;
use LVC\PHPGames\Domain\Player\PlayerInterface;

class Monkey implements PlayerInterface
{
    /** @var Command[] */
    private $availableCommands;
    /** @var ?int */
    private $fatigue;
    /** @var callable|null */
    private $sleepiness;

    public function __construct(array $availableCommands, ?int $fatigue, ?callable $sleepiness)
    {
        $this->availableCommands = $availableCommands;
        $this->fatigue = $fatigue;
        $this->sleepiness = $sleepiness;
    }

    /** @return \Generator|Command[] */
    public function play(): \Generator
    {
        while (true) {
            if ($this->fatigue !== null && --$this->fatigue < 0) {
                return;
            }
            yield $this->availableCommands[array_rand($this->availableCommands)];

            if ($this->sleepiness !== null && $sleepingUntil = call_user_func($this->sleepiness)) {
                while (is_double($sleepingUntil) && microtime(true) < $sleepingUntil) {
                    yield null;
                    usleep(1);
                }
            }
        }
    }
}
