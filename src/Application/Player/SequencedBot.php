<?php

namespace LVC\PHPGames\Application\Player;

use LVC\PHPGames\Domain\Gameplay\Command;
use LVC\PHPGames\Domain\Player\PlayerInterface;

class SequencedBot implements PlayerInterface
{
    /** @var Command[] */
    private $commandsList;

    public function __construct(array $commandsList)
    {
        $this->commandsList = $commandsList;
    }

    /** @return \Generator|Command[] */
    public function play(): \Generator
    {
        yield from $this->commandsList;
    }
}
