<?php

namespace LVC\PHPGames\Application\Player;

use LVC\PHPGames\Domain\Gameplay\Command;
use LVC\PHPGames\Domain\Player\PlayerInterface;

class Stream implements PlayerInterface
{
    /** @var resource */
    private $stream;

    public function __construct(string $streamSource)
    {
        $stream = @fopen($streamSource, 'r');
        if ($stream === false) {
            throw new \InvalidArgumentException('Failed to open stream.');
        }

        $this->stream = $stream;
    }

    /** @return \Generator|Command[] */
    public function play(): \Generator
    {
        do {
            $input = fgetc($this->stream);
            if (is_string($input) && !empty(trim($input))) {
                yield new Command(trim($input));
            }
        } while ($input !== false);
    }
}
