<?php

namespace LVC\PHPGames\Application\Sokoban;

use LVC\PHPGames\Domain\Level\Grid\Coordinates;
use LVC\PHPGames\Domain\Level\InvalidLevelException;
use LVC\PHPGames\Domain\Level\LevelInterface;
use LVC\PHPGames\Domain\Level\LevelStateInterface;

class LevelState implements LevelStateInterface
{
    /** @var Level */
    private $level;
    /** @var Coordinates */
    private $currentLocations;
    /** @var Coordinates[] */
    private $cratesLocations;
    /** @var array */
    private $state;
    /** @var string */
    private $compiledState;

    public function __construct(Level $level, Coordinates $currentLocation, array $cratesLocations)
    {
        $this->level = $level;
        $this->currentLocations = $currentLocation;
        $this->cratesLocations = $cratesLocations;

        $this->render();
    }

    /** @return Level */
    public function getLevel(): LevelInterface
    {
        return $this->level;
    }

    public function getPlayerLocation(): Coordinates
    {
        return $this->currentLocations;
    }

    public function getCratesLocations(): array
    {
        return $this->cratesLocations;
    }

    public function getCoordinatesState(Coordinates $coordinates): ?string
    {
        return $this->state[$coordinates->getY()][$coordinates->getX()] ?? null;
    }

    public function getState(): string
    {
        return $this->compiledState;
    }

    public function isVictory(): bool
    {
        return empty(array_diff($this->getCratesLocations(), $this->getLevel()->getStorageLocations()));
    }

    private function render()
    {
        for ($y = $this->getLevel()->getHeight() - 1; $y >= 0; $y--) {
            for ($x = 0; $x < $this->getLevel()->getWidth(); $x++) {
                $coordinates = (string) new Coordinates($x, $y);
                $state = Rules::STATE_EMPTY;
                if (in_array($coordinates, $this->getLevel()->getStorageLocations())) {
                    $state = Rules::STATE_STORAGE;
                }
                if (in_array($coordinates, $this->getCratesLocations())) {
                    $state = $state === Rules::STATE_STORAGE ? Rules::STATE_CRATE_OVER_STORAGE : Rules::STATE_CRATE;
                }
                if ($coordinates == (string) $this->getPlayerLocation()) {
                    if ($state === Rules::STATE_CRATE || $state === Rules::STATE_CRATE_OVER_STORAGE) {
                        throw new InvalidLevelException('A level cannot have overlapping elements.');
                    }
                    $state = $state === Rules::STATE_STORAGE ? Rules::STATE_PLAYER_OVER_STORAGE : Rules::STATE_PLAYER;
                }
                if (in_array($coordinates, $this->getLevel()->getWallsLocations())) {
                    if ($state !== Rules::STATE_EMPTY) {
                        throw new InvalidLevelException('A level cannot have overlapping elements.');
                    }
                    $state = Rules::STATE_WALL;
                }
                $this->state[$y][$x] = $state;
            }
        }

        $this->compiledState = join("\n", array_map(function ($state) { return join('', $state); }, $this->state));
    }
}
