<?php

namespace LVC\PHPGames\Application\Sokoban;

use LVC\PHPGames\Domain\Level\Grid\Coordinates;
use LVC\PHPGames\Domain\Level\Grid\GridLevel;
use LVC\PHPGames\Domain\Level\InvalidLevelException;
use LVC\PHPGames\Domain\Level\LevelInterface;
use LVC\PHPGames\Domain\Level\LevelStateInterface;

class Level implements LevelInterface
{
    use GridLevel;

    /** @var Coordinates */
    private $startingLocation;
    /** @var Coordinates[] */
    private $cratesLocations;
    /** @var Coordinates[] */
    private $storageLocations;
    /** @var Coordinates[] */
    private $wallsLocations;
    /** @var LevelState */
    private $initialLevelState;

    public function __construct(
        int $width,
        int $height,
        Coordinates $startingLocation,
        array $cratesLocations,
        array $storageLocations,
        array $wallsLocations
    ) {
        if (count(array_unique($cratesLocations)) !== count($storageLocations)) {
            throw new InvalidLevelException(
                'Level needs the same number of crates ('.count(array_unique($cratesLocations)).') than storage ('.count($storageLocations).') locations.'
            );
        }
        if (empty($cratesLocations)) {
            throw new InvalidLevelException(
                'Level needs at least one crate and storage location.'
            );
        }
        /** @var Coordinates $location */
        foreach (array_merge([$startingLocation], $cratesLocations, $storageLocations, $wallsLocations) as $location) {
            if (!$location->checkWithin($width, $height)) {
                throw new InvalidLevelException(
                    'Location ('.$location.') is out of bound (0-'.($width - 1).' x 0-'.($height - 1).').'
                );
            }
        }

        $this->width = $width;
        $this->height = $height;

        $this->startingLocation = $startingLocation;
        $this->cratesLocations = $cratesLocations;
        $this->storageLocations = $storageLocations;
        $this->wallsLocations = $wallsLocations;

        $this->initialLevelState = new LevelState($this, $this->startingLocation, $this->cratesLocations);
    }

    public function getStartingLocation(): Coordinates
    {
        return $this->startingLocation;
    }

    /** @return Coordinates[]*/
    public function getCratesLocations(): array
    {
        return $this->cratesLocations;
    }

    /** @return Coordinates[]*/
    public function getStorageLocations(): array
    {
        return $this->storageLocations;
    }

    /** @return Coordinates[]*/
    public function getWallsLocations(): array
    {
        return $this->wallsLocations;
    }

    public function start(): LevelStateInterface
    {
        return $this->initialLevelState;
    }
}
