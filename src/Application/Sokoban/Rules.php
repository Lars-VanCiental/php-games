<?php

namespace LVC\PHPGames\Application\Sokoban;

use LVC\PHPGames\Domain\Gameplay\Command;
use LVC\PHPGames\Domain\Gameplay\RulesInterface;
use LVC\PHPGames\Domain\Level\Grid\Coordinates;
use LVC\PHPGames\Domain\Level\LevelStateInterface;

class Rules implements RulesInterface
{
    const COMMAND_MOVE_UP = 'z';
    const COMMAND_MOVE_DOWN = 's';
    const COMMAND_MOVE_RIGHT = 'd';
    const COMMAND_MOVE_LEFT = 'q';

    const ALLOWED_COMMANDS = [
        self::COMMAND_MOVE_UP,
        self::COMMAND_MOVE_DOWN,
        self::COMMAND_MOVE_RIGHT,
        self::COMMAND_MOVE_LEFT,
    ];

    const STATE_EMPTY = '-';
    const STATE_WALL = '#';
    const STATE_STORAGE = '.';
    const STATE_CRATE = '$';
    const STATE_CRATE_OVER_STORAGE = '*';
    const STATE_PLAYER = '@';
    const STATE_PLAYER_OVER_STORAGE = '+';

    public function getAvailableCommands(): array
    {
        return array_map(function (string $command) { return new Command($command); }, self::ALLOWED_COMMANDS);
    }

    public function play(LevelStateInterface $initialLevelState): \Generator
    {
        if (!($initialLevelState instanceof LevelState)) {
            throw new \InvalidArgumentException();
        }

        $levelState = $initialLevelState;

        while (true) {
            if ($levelState->isVictory()) {
                return $levelState;
            }
            $command = yield $levelState;

            if (!$command) {
                continue;
            }

            $currentPlayerLocation = $levelState->getPlayerLocation();

            $nextPlayerLocation = self::getNextLocation($currentPlayerLocation, $command);
            if (!$nextPlayerLocation->checkWithin($levelState->getLevel()->getWidth(), $levelState->getLevel()->getHeight())) {
                continue;
            }

            $cratesLocations = $levelState->getCratesLocations();
            switch ($levelState->getCoordinatesState($nextPlayerLocation)) {
                case self::STATE_CRATE:
                case self::STATE_CRATE_OVER_STORAGE:
                    $nextCrateLocation = self::getNextLocation($nextPlayerLocation, $command);

                    if (!$nextCrateLocation->checkWithin($levelState->getLevel()->getWidth(), $levelState->getLevel()->getHeight())) {
                        continue;
                    }

                    switch ($levelState->getCoordinatesState($nextCrateLocation)) {
                        case self::STATE_STORAGE:
                        case self::STATE_EMPTY:
                            $cratesLocations[array_search($nextPlayerLocation, $cratesLocations)] = $nextCrateLocation;
                            $currentPlayerLocation = $nextPlayerLocation;
                            break;
                    }
                    break;
                case self::STATE_EMPTY:
                case self::STATE_STORAGE:
                    $currentPlayerLocation = $nextPlayerLocation;
                    break;
            }

            $levelState = new LevelState($levelState->getLevel(), $currentPlayerLocation, $cratesLocations);
        }
    }

    private static function getNextLocation(Coordinates $currentLocation, Command $command): Coordinates
    {
        switch ($command->getCommand()) {
            case self::COMMAND_MOVE_UP:
                return new Coordinates($currentLocation->getX(), $currentLocation->getY() + 1);
            case self::COMMAND_MOVE_DOWN:
                return new Coordinates($currentLocation->getX(), $currentLocation->getY() - 1);
            case self::COMMAND_MOVE_RIGHT:
                return new Coordinates($currentLocation->getX() + 1, $currentLocation->getY());
            case self::COMMAND_MOVE_LEFT:
                return new Coordinates($currentLocation->getX() - 1, $currentLocation->getY());
        }

        return $currentLocation;
    }
}
