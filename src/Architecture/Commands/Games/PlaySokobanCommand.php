<?php

namespace LVC\PHPGames\Architecture\Commands\Games;

use LVC\PHPGames\Application\Sokoban\Rules;
use LVC\PHPGames\Architecture\Commands\AbstractPlayCommand;
use LVC\PHPGames\Architecture\Sokoban\LevelFactory;
use LVC\PHPGames\Domain\Gameplay\RulesInterface;
use LVC\PHPGames\Domain\Level\LevelFactoryInterface;
use LVC\PHPGames\Domain\Level\LevelStateInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class PlaySokobanCommand extends AbstractPlayCommand
{
    /** @var Rules */
    private $rules;
    /** @var LevelFactory */
    private $levelFactory;

    protected function configure()
    {
        $this->setName('play:sokoban');

        parent::configure();
    }

    protected function setup(InputInterface $input, SymfonyStyle $io): void
    {
        $this->rules = new Rules();
        $this->levelFactory = new LevelFactory(__DIR__.'/../../../../config/sokoban/levels');
    }

    protected function getRules(): RulesInterface
    {
        return $this->rules;
    }

    protected function getLevelFactory(): LevelFactoryInterface
    {
        return $this->levelFactory;
    }

    protected function formatLevelState(LevelStateInterface $levelState): string
    {
        return str_replace(
            [
                Rules::STATE_EMPTY,
                Rules::STATE_WALL,
                Rules::STATE_STORAGE,
                Rules::STATE_CRATE,
                Rules::STATE_CRATE_OVER_STORAGE,
                Rules::STATE_PLAYER,
                Rules::STATE_PLAYER_OVER_STORAGE,
            ],
            [
                ' ',
                '<fg=white>#</>',
                '<fg=yellow>.</>',
                '<fg=red>$</>',
                '<fg=yellow>$</>',
                '<fg=green>@</>',
                '<fg=yellow>@</>',
            ],
            $levelState->getState()
        );
    }
}
