<?php

namespace LVC\PHPGames\Architecture\Commands;

use LVC\PHPGames\Application\Player\Monkey;
use LVC\PHPGames\Application\Player\SequencedBot;
use LVC\PHPGames\Application\Player\Stream;
use LVC\PHPGames\Domain\Gameplay\Game;
use LVC\PHPGames\Domain\Gameplay\RulesInterface;
use LVC\PHPGames\Domain\Level\LevelFactoryInterface;
use LVC\PHPGames\Domain\Level\LevelStateInterface;
use LVC\PHPGames\Domain\Player\PlayerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

abstract class AbstractPlayCommand extends Command
{
    const ARGUMENT_LEVEL_NAME = 'level-name';

    const OPTION_PLAYER_TYPE = 'player-type';
    const PLAYER_TYPE_STREAM = 'stream';
    const PLAYER_TYPE_MONKEY = 'monkey';
    const PLAYER_TYPE_SEQUENCED_BOT = 'sequenced-bot';
    const PLAYER_TYPES = [
        self::PLAYER_TYPE_STREAM,
        self::PLAYER_TYPE_MONKEY,
        self::PLAYER_TYPE_SEQUENCED_BOT,
    ];
    // input option
    const OPTION_STREAM_SOURCE = 'stream-source';
    // monkey player option
    const OPTION_MONKEY_FATIGUE = 'monkey-fatigue';
    const OPTION_MONKEY_SLEEPINESS = 'monkey-sleepiness';
    // sequenced bot option
    const OPTION_SEQUENCED_BOT_SEQUENCE = 'bot-sequence';

    const OPTION_REFRESH_RATE = 'refresh-rate';

    protected function configure()
    {
        $this->addArgument(
            self::ARGUMENT_LEVEL_NAME,
            InputArgument::REQUIRED,
            'Name of the level.'
        );

        $this->addOption(
            self::OPTION_PLAYER_TYPE,
            null,
            InputOption::VALUE_REQUIRED,
            'Type of player. Available: "'.join('", "', self::PLAYER_TYPES).'".',
            self::PLAYER_TYPE_STREAM
        );

        $this->addOption(
            self::OPTION_STREAM_SOURCE,
            null,
            InputOption::VALUE_REQUIRED,
            '[player-type="'.self::PLAYER_TYPE_STREAM.'"] Source of the stream.',
            'php://stdin'
        );

        $this->addOption(
            self::OPTION_MONKEY_FATIGUE,
            null,
            InputOption::VALUE_REQUIRED,
            '[player-type="'.self::PLAYER_TYPE_MONKEY.'"] Maximum number of command to be executed.',
            '500'
        );
        $this->addOption(
            self::OPTION_MONKEY_SLEEPINESS,
            null,
            InputOption::VALUE_REQUIRED,
            '[player-type="'.self::PLAYER_TYPE_MONKEY.'"] Time (in microseconds) waited between commands, can be a range (100-1000).',
            null
        );

        $this->addOption(
            self::OPTION_SEQUENCED_BOT_SEQUENCE,
            null,
            InputOption::VALUE_REQUIRED,
            '[player-type="'.self::PLAYER_TYPE_SEQUENCED_BOT.'"] Sequence of commands that will be played.',
            null
        );

        $this->addOption(
            self::OPTION_REFRESH_RATE,
            null,
            InputOption::VALUE_REQUIRED,
            '[player-type="'.self::PLAYER_TYPE_SEQUENCED_BOT.'"] Delay (in microseconds) between two view refreshment.',
            null
        );

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $this->setup($input, $io);

        try {
            $levelName = $input->getArgument(self::ARGUMENT_LEVEL_NAME);
            if (empty($levelName) || !is_string($levelName)) {
                throw new \InvalidArgumentException('Invalid level name given.');
            }

            $game = new Game($this->getRules(), $this->getLevelFactory());

            $gameSession = $game->startSession($this->getPlayer($input, $io), $levelName);
        } catch (\Exception $e) {
            $io->error('Invalid configuration: '.$e->getMessage());

            return;
        }

        $refreshRate = intval($input->getOption(self::OPTION_REFRESH_RATE)) / 1000000;

        $displayDelay = null;
        $previousLevelState = null;
        /** @var LevelStateInterface $levelState */
        foreach ($gameSession->play() as $levelState) {
            if ($levelState instanceof LevelStateInterface && $previousLevelState !== $levelState->getState()) {
                if ($displayDelay === null || $displayDelay < microtime(true)) {
                    $displayDelay = microtime(true) + $refreshRate;
                    $previousLevelState = $levelState->getState();

                    $this->displayLevelState($levelState, $io);
                }
            }
        }

        if (empty($levelState)) {
            $io->error('Something might have gone wrong :/');

            return;
        }

        $this->displayLevelState($levelState, $io);

        $levelState->isVictory() ? $io->success('Victory') : $io->warning('Failed');
    }

    abstract protected function setup(InputInterface $input, SymfonyStyle $io): void;

    protected function getPlayer(InputInterface $input, SymfonyStyle $io): PlayerInterface
    {
        switch ($input->getOption(self::OPTION_PLAYER_TYPE)) {
            case self::PLAYER_TYPE_MONKEY:
                return $this->createMonkeyPlayer($input);
            case self::PLAYER_TYPE_STREAM:
                return $this->createStreamPlayer($input);
            case self::PLAYER_TYPE_SEQUENCED_BOT:
                return $this->createSequenceBotPlayer($input);
            default:
                throw new \InvalidArgumentException('Invalid player type given.');
        }
    }

    protected function createStreamPlayer(InputInterface $input): Stream
    {
        $stream = $input->getOption(self::OPTION_STREAM_SOURCE);
        if (!is_string($stream)) {
            throw new \InvalidArgumentException('Invalid stream given.');
        }

        return new Stream($stream);
    }

    protected function createMonkeyPlayer(InputInterface $input): Monkey
    {
        $monkeyFatigue = $input->hasOption(self::OPTION_MONKEY_FATIGUE) ? intval($input->getOption(self::OPTION_MONKEY_FATIGUE)) : null;
        $sleepiness = $input->hasOption(self::OPTION_MONKEY_SLEEPINESS) ? $input->getOption(self::OPTION_MONKEY_SLEEPINESS) : null;

        return new Monkey(
            $this->getRules()->getAvailableCommands(),
            $monkeyFatigue,
            $sleepiness ? function () use ($sleepiness) {
                if (is_numeric($sleepiness)) {
                    return microtime(true);
                }
                $matches = [];
                if (is_string($sleepiness) && preg_match('#(?<minRange>\d+)-(?<maxRange>\d+)#', $sleepiness, $matches)) {
                    return microtime(true) + rand($matches['minRange'], $matches['maxRange']) / 1000000;
                }

                throw new \InvalidArgumentException('Invalid fatigue given.');
            } : null
        );
    }

    protected function createSequenceBotPlayer(InputInterface $input): SequencedBot
    {
        $sequence = $input->getOption(self::OPTION_SEQUENCED_BOT_SEQUENCE);
        if (!is_string($sequence)) {
            throw new \InvalidArgumentException('Invalid command sequence given.');
        }

        return new SequencedBot(
            array_map(
                function (string $command) {
                    return new \LVC\PHPGames\Domain\Gameplay\Command($command);
                },
                str_split($sequence)
            )
        );
    }

    abstract protected function getRules(): RulesInterface;

    abstract protected function getLevelFactory(): LevelFactoryInterface;

    protected function displayLevelState(LevelStateInterface $levelState, SymfonyStyle $io): void
    {
        $io->writeLn(sprintf("\033\143"));
        $io->writeln($this->formatLevelState($levelState));
    }

    abstract protected function formatLevelState(LevelStateInterface $levelState): string;
}
