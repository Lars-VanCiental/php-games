<?php

namespace LVC\PHPGames\Architecture;

trait LevelLoaderTrait
{
    /** @var string */
    private $levelsDirectory;
    /** @var string */
    private $levelsExtension;

    private function configure(string $levelsDirectory, string $levelsExtension)
    {
        if (!is_dir($levelsDirectory)) {
            throw new \InvalidArgumentException('Levels directory "'.$levelsDirectory.'" does not exist!');
        }

        $this->levelsDirectory = $levelsDirectory;
        $this->levelsExtension = $levelsExtension;
    }

    private function getLevelContent(string $levelName): string
    {
        $levelContent = @file_get_contents(
            join(
                '/',
                [
                    trim($this->levelsDirectory, '/'),
                    $levelName.$this->levelsExtension,
                ]
            )
        );

        if (!is_string($levelContent)) {
            throw new \InvalidArgumentException('Levels file "'.$levelName.'" does not exist!');
        }

        return $levelContent;
    }
}
