<?php

namespace LVC\PHPGames\Architecture\Sokoban;

use LVC\PHPGames\Application\Sokoban\Level;
use LVC\PHPGames\Application\Sokoban\Rules;
use LVC\PHPGames\Architecture\LevelLoaderTrait;
use LVC\PHPGames\Domain\Level\Grid\Coordinates;
use LVC\PHPGames\Domain\Level\InvalidLevelException;
use LVC\PHPGames\Domain\Level\LevelFactoryInterface;
use LVC\PHPGames\Domain\Level\LevelInterface;

class LevelFactory implements LevelFactoryInterface
{
    use LevelLoaderTrait;

    const SOKOBAN_LEVELS_FILE_EXTENSION = '.xsb';

    public function __construct(string $levelsDirectory)
    {
        $this->configure($levelsDirectory, self::SOKOBAN_LEVELS_FILE_EXTENSION);
    }

    public function loadLevel(string $levelName): LevelInterface
    {
        $levelContent = explode("\n", $this->getLevelContent($levelName));

        $y = ($height = count($levelContent)) - 1;
        $width = 0;

        $startingLocation = null;
        $cratesLocations = [];
        $storageLocations = [];
        $wallsLocations = [];

        foreach ($levelContent as $levelLine) {
            $levelStates = str_split($levelLine);

            $x = 0;
            foreach ($levelStates as $levelState) {
                $coordinate = new Coordinates($x, $y);

                switch ($levelState) {
                    case Rules::STATE_PLAYER:
                        $startingLocation = $coordinate;
                        break;
                    case Rules::STATE_CRATE:
                        $cratesLocations[] = $coordinate;
                        break;
                    case Rules::STATE_STORAGE:
                        $storageLocations[] = $coordinate;
                        break;
                    case Rules::STATE_WALL:
                        $wallsLocations[] = $coordinate;
                        break;
                    case Rules::STATE_CRATE_OVER_STORAGE:
                        $cratesLocations[] = $coordinate;
                        $storageLocations[] = $coordinate;
                        break;
                    case Rules::STATE_PLAYER_OVER_STORAGE:
                        $startingLocation = $coordinate;
                        $storageLocations[] = $coordinate;
                        break;
                }

                $x++;
            }

            $width = max([$width, $x]);
            $y--;
        }

        if ($startingLocation === null) {
            throw new InvalidLevelException('Missing starting location in level "'.$levelName.'".');
        }

        return new Level(
            $width,
            $height,
            $startingLocation,
            $cratesLocations,
            $storageLocations,
            $wallsLocations
        );
    }
}
