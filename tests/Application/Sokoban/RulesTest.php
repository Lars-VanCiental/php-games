<?php

namespace LVC\PHPGamesTest\Application\Sokoban;

use LVC\PHPGames\Application\Sokoban\Level;
use LVC\PHPGames\Application\Sokoban\LevelState;
use LVC\PHPGames\Application\Sokoban\Rules;
use LVC\PHPGames\Domain\Gameplay\Command;
use LVC\PHPGames\Domain\Level\Grid\Coordinates;
use LVC\PHPGames\Domain\Level\LevelStateInterface;
use LVC\PHPGamesTest\Domain\LevelStateInterfaceMock;
use PHPUnit\Framework\TestCase;

class RulesTest extends TestCase
{
    use LevelStateInterfaceMock;

    public function testExceptionWhenNotSokobanLevelState()
    {
        $rules = new Rules();

        $this->expectException(\InvalidArgumentException::class);

        $generator = $rules->play($this->getLevelStateMock('test'));
        $generator->current(); // needed to trigger the exception
    }

    /** @dataProvider getCommandPlayTestCases */
    public function testPlayIndividualMove(LevelState $levelState, ?Command $command, string $expectedState)
    {
        $rules = new Rules();

        $generator = $rules->play($levelState);

        $generator->send($command); // yield initial state
        $newLevelState = $generator->send(null);

        $this->assertInstanceOf(LevelState::class, $newLevelState);
        $this->assertEquals($expectedState, $newLevelState->getState());
    }

    public function getCommandPlayTestCases(): \Generator
    {
        // locked
        $levelLocked = new Level(
            5,
            5,
            new Coordinates(2, 2),
            [new Coordinates(0, 0)],
            [new Coordinates(4, 4)],
            [
                new Coordinates(1, 1),
                new Coordinates(1, 2),
                new Coordinates(1, 3),
                new Coordinates(2, 3),
                new Coordinates(3, 3),
                new Coordinates(3, 2),
                new Coordinates(3, 1),
                new Coordinates(2, 1),
            ]
        );
        $stateLocked = <<<STATE
----.
-###-
-#@#-
-###-
$----
STATE;

        yield 'locked-up' => [
            $levelLocked->start(),
            new Command(Rules::COMMAND_MOVE_UP),
            $stateLocked,
        ];

        yield 'locked-down' => [
            $levelLocked->start(),
            new Command(Rules::COMMAND_MOVE_DOWN),
            $stateLocked,
        ];

        yield 'locked-right' => [
            $levelLocked->start(),
            new Command(Rules::COMMAND_MOVE_RIGHT),
            $stateLocked,
        ];

        yield 'locked-left' => [
            $levelLocked->start(),
            new Command(Rules::COMMAND_MOVE_LEFT),
            $stateLocked,
        ];

        // out of bound
        $levelStateStrandedTopLeft = new LevelState($levelLocked, new Coordinates(0, 4), $levelLocked->getCratesLocations());
        $stateStrandedTopLeft = <<<STATE
@---.
-###-
-#-#-
-###-
$----
STATE;

        yield 'out-of-bound-up' => [
            $levelStateStrandedTopLeft,
            new Command(Rules::COMMAND_MOVE_UP),
            $stateStrandedTopLeft,
        ];

        yield 'out-of-bound-left' => [
            $levelStateStrandedTopLeft,
            new Command(Rules::COMMAND_MOVE_LEFT),
            $stateStrandedTopLeft,
        ];

        $levelStateStrandedDownRight = new LevelState($levelLocked, new Coordinates(4, 0), $levelLocked->getCratesLocations());
        $stateStrandedDownRight = <<<STATE
----.
-###-
-#-#-
-###-
$---@
STATE;

        yield 'out-of-bound-down' => [
            $levelStateStrandedDownRight,
            new Command(Rules::COMMAND_MOVE_DOWN),
            $stateStrandedDownRight,
        ];

        yield 'out-of-bound-right' => [
            $levelStateStrandedDownRight,
            new Command(Rules::COMMAND_MOVE_RIGHT),
            $stateStrandedDownRight,
        ];

        // going over a storage
        $levelOverStorage = new Level(
            5,
            5,
            new Coordinates(2, 2),
            [new Coordinates(1, 1), new Coordinates(1, 3), new Coordinates(3, 3), new Coordinates(3, 1)],
            [new Coordinates(1, 2), new Coordinates(2, 3), new Coordinates(3, 2), new Coordinates(2, 1)],
            []
        );

        yield 'over-storage-up' => [
            $levelOverStorage->start(),
            new Command(Rules::COMMAND_MOVE_UP),
            <<<STATE
-----
-$+$-
-.-.-
-$.$-
-----
STATE
        ];

        yield 'over-storage-down' => [
            $levelOverStorage->start(),
            new Command(Rules::COMMAND_MOVE_DOWN),
            <<<STATE
-----
-$.$-
-.-.-
-$+$-
-----
STATE
        ];

        yield 'over-storage-right' => [
            $levelOverStorage->start(),
            new Command(Rules::COMMAND_MOVE_RIGHT),
            <<<STATE
-----
-$.$-
-.-+-
-$.$-
-----
STATE
        ];

        yield 'over-storage-left' => [
            $levelOverStorage->start(),
            new Command(Rules::COMMAND_MOVE_LEFT),
            <<<STATE
-----
-$.$-
-+-.-
-$.$-
-----
STATE
        ];

        // push a crate
        $levelPushCrate = new Level(
            5,
            5,
            new Coordinates(2, 2),
            [new Coordinates(1, 2), new Coordinates(2, 3), new Coordinates(3, 2), new Coordinates(2, 1)],
            [new Coordinates(1, 1), new Coordinates(1, 3), new Coordinates(3, 3), new Coordinates(3, 1)],
            []
        );

        yield 'push-crate-up' => [
            $levelPushCrate->start(),
            new Command(Rules::COMMAND_MOVE_UP),
            <<<STATE
--$--
-.@.-
-$-$-
-.$.-
-----
STATE
        ];

        yield 'push-crate-down' => [
            $levelPushCrate->start(),
            new Command(Rules::COMMAND_MOVE_DOWN),
            <<<STATE
-----
-.$.-
-$-$-
-.@.-
--$--
STATE
        ];

        yield 'push-crate-right' => [
            $levelPushCrate->start(),
            new Command(Rules::COMMAND_MOVE_RIGHT),
            <<<STATE
-----
-.$.-
-$-@$
-.$.-
-----
STATE
        ];

        yield 'push-crate-left' => [
            $levelPushCrate->start(),
            new Command(Rules::COMMAND_MOVE_LEFT),
            <<<STATE
-----
-.$.-
$@-$-
-.$.-
-----
STATE
        ];

        // blocked by a crate
        $levelPushCrate = new Level(
            5,
            5,
            new Coordinates(2, 2),
            [new Coordinates(1, 2), new Coordinates(2, 3), new Coordinates(3, 2), new Coordinates(2, 1)],
            [new Coordinates(1, 1), new Coordinates(1, 3), new Coordinates(3, 3), new Coordinates(3, 1)],
            [new Coordinates(0, 2), new Coordinates(2, 4), new Coordinates(4, 2), new Coordinates(2, 0)]
        );

        yield 'blocked-crate-up' => [
            $levelPushCrate->start(),
            new Command(Rules::COMMAND_MOVE_UP),
            <<<STATE
--#--
-.$.-
#$@$#
-.$.-
--#--
STATE
        ];

        yield 'blocked-crate-down' => [
            $levelPushCrate->start(),
            new Command(Rules::COMMAND_MOVE_DOWN),
            <<<STATE
--#--
-.$.-
#$@$#
-.$.-
--#--
STATE
        ];

        yield 'blocked-crate-right' => [
            $levelPushCrate->start(),
            new Command(Rules::COMMAND_MOVE_RIGHT),
            <<<STATE
--#--
-.$.-
#$@$#
-.$.-
--#--
STATE
        ];

        yield 'blocked-crate-left' => [
            $levelPushCrate->start(),
            new Command(Rules::COMMAND_MOVE_LEFT),
            <<<STATE
--#--
-.$.-
#$@$#
-.$.-
--#--
STATE
        ];
    }

    public function testPlayOverWhenVictory()
    {
        $level = new Level(
            3,
            1,
            new Coordinates(0, 0),
            [new Coordinates(1, 0)],
            [new Coordinates(2, 0)],
            []
        );

        $rules = new Rules();

        $generator = $rules->play($level->start());
        $this->assertTrue($generator->valid());

        /** @var LevelStateInterface $levelState */
        $levelState = $generator->current();
        $this->assertTrue($generator->valid());
        $this->assertFalse($levelState->isVictory());

        $levelState = $generator->send(new Command(Rules::COMMAND_MOVE_RIGHT));
        $this->assertFalse($generator->valid());
        $this->assertNull($levelState);

        $levelState = $generator->getReturn();
        $this->assertTrue($levelState->isVictory());
    }
}
