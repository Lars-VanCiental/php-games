<?php

namespace LVC\PHPGamesTest\Application\Sokoban;

use LVC\PHPGames\Application\Sokoban\Level;
use LVC\PHPGames\Application\Sokoban\LevelState;
use LVC\PHPGames\Domain\Level\Grid\Coordinates;
use LVC\PHPGames\Domain\Level\InvalidLevelException;
use PHPUnit\Framework\TestCase;

class LevelTest extends TestCase
{
    public function testExceptionWhenDifferentNumberOfCratesAndStorage()
    {
        $this->expectException(InvalidLevelException::class);
        $this->expectExceptionMessage('Level needs the same number of crates (2) than storage (1) locations.');

        new Level(
            10,
            10,
            new Coordinates(5, 5),
            [new Coordinates(2, 3), new Coordinates(2, 4)],
            [new Coordinates(3, 4)],
            [new Coordinates(1, 1)]
        );
    }

    public function testExceptionWhenMissingCratesAndStorage()
    {
        $this->expectException(InvalidLevelException::class);
        $this->expectExceptionMessage('Level needs at least one crate and storage location.');

        new Level(
            10,
            10,
            new Coordinates(5, 5),
            [],
            [],
            [new Coordinates(1, 1)]
        );
    }

    /** @dataProvider getOutOfRangeTestCases */
    public function testOutOfRangeLocations(
        Coordinates $startingLocation,
        array $cratesLocations,
        array $storageLocation,
        array $wallsLocation
    ) {
        $this->expectException(InvalidLevelException::class);
        $this->expectExceptionMessage('Location (10,10) is out of bound (0-9 x 0-9).');

        new Level(
            10,
            10,
            $startingLocation,
            $cratesLocations,
            $storageLocation,
            $wallsLocation
        );
    }

    public function getOutOfRangeTestCases(): \Generator
    {
        yield 'starting-position' => [
            new Coordinates(10, 10),
            [new Coordinates(1, 1), new Coordinates(1, 2), new Coordinates(1, 3)],
            [new Coordinates(2, 1), new Coordinates(2, 2), new Coordinates(2, 3)],
            [new Coordinates(3, 1), new Coordinates(3, 2), new Coordinates(3, 3)],
        ];

        yield 'first-crate' => [
            new Coordinates(5, 5),
            [new Coordinates(10, 10), new Coordinates(1, 2), new Coordinates(1, 3)],
            [new Coordinates(2, 1), new Coordinates(2, 2), new Coordinates(2, 3)],
            [new Coordinates(3, 1), new Coordinates(3, 2), new Coordinates(3, 3)],
        ];

        yield 'second-storage' => [
            new Coordinates(5, 5),
            [new Coordinates(1, 1), new Coordinates(1, 2), new Coordinates(1, 3)],
            [new Coordinates(2, 1), new Coordinates(10, 10), new Coordinates(2, 3)],
            [new Coordinates(3, 1), new Coordinates(3, 2), new Coordinates(3, 3)],
        ];

        yield 'third-wall' => [
            new Coordinates(5, 5),
            [new Coordinates(1, 1), new Coordinates(1, 2), new Coordinates(1, 3)],
            [new Coordinates(2, 1), new Coordinates(2, 2), new Coordinates(2, 3)],
            [new Coordinates(3, 1), new Coordinates(3, 2), new Coordinates(10, 10)],
        ];
    }

    public function testStart()
    {
        $level = new Level(
            10,
            10,
            new Coordinates(5, 5),
            [new Coordinates(1, 1), new Coordinates(1, 2)],
            [new Coordinates(2, 1), new Coordinates(2, 2)],
            [new Coordinates(3, 3)]
        );

        /** @var LevelState $levelState */
        $levelState = $level->start();

        $this->assertInstanceOf(LevelState::class, $levelState);
        $this->assertEquals(new Coordinates(5, 5), $levelState->getPlayerLocation());
        $this->assertEquals([new Coordinates(1, 1), new Coordinates(1, 2)], $levelState->getCratesLocations());
    }
}
