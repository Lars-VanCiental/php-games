<?php

namespace LVC\PHPGamesTest\Application\Sokoban;

use LVC\PHPGames\Application\Sokoban\Level;
use LVC\PHPGames\Application\Sokoban\LevelState;
use LVC\PHPGames\Application\Sokoban\Rules;
use LVC\PHPGames\Domain\Level\Grid\Coordinates;
use PHPUnit\Framework\TestCase;

class LevelStateTest extends TestCase
{
    /** @dataProvider getCoordinatesStateTestCases */
    public function testGetCoordinatesState(
        Level $level,
        Coordinates $currentLocation,
        array $cratesLocation,
        Coordinates $testedCoordinates,
        string $expectedState
    ) {
        $levelState = new LevelState($level, $currentLocation, $cratesLocation);

        $this->assertEquals($expectedState, $levelState->getCoordinatesState($testedCoordinates));
    }

    public function getCoordinatesStateTestCases(): \Generator
    {
        $level = $this->getLevel();

        yield 'level-initial-state-0,0' => [
            $level,
            $level->getStartingLocation(),
            $level->getCratesLocations(),
            new Coordinates(0, 0),
            Rules::STATE_EMPTY,
        ];

        yield 'level-initial-state-2,1' => [
            $level,
            $level->getStartingLocation(),
            $level->getCratesLocations(),
            new Coordinates(2, 1),
            Rules::STATE_WALL,
        ];

        yield 'level-initial-state-4,2' => [
            $level,
            $level->getStartingLocation(),
            $level->getCratesLocations(),
            new Coordinates(4, 2),
            Rules::STATE_WALL,
        ];

        yield 'level-initial-state-starting-position' => [
            $level,
            $level->getStartingLocation(),
            $level->getCratesLocations(),
            new Coordinates(5, 1),
            Rules::STATE_PLAYER,
        ];

        yield 'level-initial-state-crate' => [
            $level,
            $level->getStartingLocation(),
            $level->getCratesLocations(),
            new Coordinates(8, 1),
            Rules::STATE_CRATE,
        ];

        yield 'level-initial-state-storage' => [
            $level,
            $level->getStartingLocation(),
            $level->getCratesLocations(),
            new Coordinates(12, 1),
            Rules::STATE_STORAGE,
        ];

        yield 'level-initial-state-storage' => [
            $level,
            $level->getStartingLocation(),
            $level->getCratesLocations(),
            new Coordinates(12, 1),
            Rules::STATE_STORAGE,
        ];

        // move player over storage location

        yield 'player-on-storage' => [
            $level,
            new Coordinates(12, 1),
            $level->getCratesLocations(),
            new Coordinates(12, 1),
            Rules::STATE_PLAYER_OVER_STORAGE,
        ];

        // move crate over storage location

        yield 'crate-on-storage' => [
            $level,
            $level->getStartingLocation(),
            [new Coordinates(12, 1)],
            new Coordinates(12, 1),
            Rules::STATE_CRATE_OVER_STORAGE,
        ];
    }

    /** @dataProvider getStateTestCases */
    public function testGetState(
        Level $level,
        Coordinates $currentLocation,
        array $cratesLocation,
        string $expectedState
    ) {
        $levelState = new LevelState($level, $currentLocation, $cratesLocation);

        $this->assertEquals($expectedState, $levelState->getState());
    }

    public function getStateTestCases(): \Generator
    {
        $level = $this->getLevel();
        $state = <<<STATE
---#####-####
--##-@--$---.
----#########
STATE;

        yield 'initial-state' => [
            $level,
            $level->getStartingLocation(),
            $level->getCratesLocations(),
            $state,
        ];

        // move player over storage location
        $state = <<<STATE
---#####-####
--##----$---+
----#########
STATE;

        yield 'player-on-storage' => [
            $level,
            new Coordinates(12, 1),
            $level->getCratesLocations(),
            $state,
        ];

        // move crate over storage location
        $state = <<<STATE
---#####-####
--##-@------*
----#########
STATE;

        yield 'crate-on-storage' => [
            $level,
            $level->getStartingLocation(),
            [new Coordinates(12, 1)],
            $state,
        ];
    }

    private function getLevel(): Level
    {
        return new Level(
            13,
            3,
            new Coordinates(5, 1),
            [new Coordinates(8, 1)],
            [new Coordinates(12, 1)],
            [
                new Coordinates(4, 0),
                new Coordinates(5, 0),
                new Coordinates(6, 0),
                new Coordinates(7, 0),
                new Coordinates(8, 0),
                new Coordinates(9, 0),
                new Coordinates(10, 0),
                new Coordinates(11, 0),
                new Coordinates(12, 0),
                new Coordinates(2, 1),
                new Coordinates(3, 1),
                new Coordinates(3, 2),
                new Coordinates(4, 2),
                new Coordinates(5, 2),
                new Coordinates(6, 2),
                new Coordinates(7, 2),
                new Coordinates(9, 2),
                new Coordinates(10, 2),
                new Coordinates(11, 2),
                new Coordinates(12, 2),
            ]
        );
    }
}
