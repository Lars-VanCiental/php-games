<?php

namespace LVC\PHPGamesTest\Application\Player;

use LVC\PHPGames\Application\Player\Monkey;
use LVC\PHPGames\Domain\Gameplay\Command;
use LVC\PHPGamesTest\Domain\CommandMock;
use PHPUnit\Framework\TestCase;

class MonkeyTest extends TestCase
{
    use CommandMock;

    public function testPlay()
    {
        $availableCommands = [$this->getCommandMock('a'), $this->getCommandMock('b')];

        $monkey = new Monkey($availableCommands, null, null);

        $generator = $monkey->play();

        $this->assertInstanceOf(\Generator::class, $generator);
        $this->assertInstanceOf(Command::class, $generator->current());
        $this->assertTrue(in_array($generator->current(), $availableCommands));
    }

    public function testPlayButBored()
    {
        $availableCommands = [$this->getCommandMock('a'), $this->getCommandMock('b')];

        $monkey = new Monkey($availableCommands, 5, null);

        $generator = $monkey->play();

        $count = 0;
        foreach ($generator as $command) {
            $this->assertInstanceOf(Command::class, $command);
            $count++;
        }
        $this->assertEquals(5, $count);
    }

    public function testPlayButSleepy()
    {
        $availableCommands = [$this->getCommandMock('a'), $this->getCommandMock('b')];

        $monkey = new Monkey($availableCommands, null, function () { return microtime(true) + 0.5; });

        $generator = $monkey->play();

        // first yield is a command
        $this->assertInstanceOf(Command::class, $generator->current());
        $generator->next();
        // next yields are null since we did not wait enough
        $this->assertNull($generator->current());
        $generator->next();
        $this->assertNull($generator->current());
        // sleep some time
        sleep(1);
        // next yield is a command
        $generator->next();
        $this->assertInstanceOf(Command::class, $generator->current());
        $generator->next();
        $this->assertNull($generator->current());
    }
}
