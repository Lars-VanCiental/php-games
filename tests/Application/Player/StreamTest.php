<?php

namespace LVC\PHPGamesTest\Application\Player;

use LVC\PHPGames\Application\Player\Stream;
use LVC\PHPGames\Domain\Gameplay\Command;
use LVC\PHPGamesTest\Domain\CommandMock;
use PHPUnit\Framework\TestCase;

class StreamTest extends TestCase
{
    use CommandMock;

    public function testExceptionWithNotFoundStream()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Failed to open stream.');

        new Stream(__DIR__.'/fixture-does-not-exist');
    }

    public function testPlayButBored()
    {
        $streamed = new \LVC\PHPGames\Application\Player\Stream(__DIR__.'/fixture-stremed.txt');

        $generator = $streamed->play();

        $this->assertTrue($generator->valid());
        $this->assertEquals(new Command('a'), $generator->current());
        $generator->next();

        $this->assertTrue($generator->valid());
        $this->assertEquals(new Command('b'), $generator->current());
        $generator->next();

        $this->assertTrue($generator->valid());
        $this->assertEquals(new Command('c'), $generator->current());
        $generator->next();

        $this->assertTrue($generator->valid());
        $this->assertEquals(new Command('d'), $generator->current());
        $generator->next();

        $this->assertTrue($generator->valid());
        $this->assertEquals(new Command('e'), $generator->current());
        $generator->next();

        $this->assertTrue($generator->valid());
        $this->assertEquals(new Command('f'), $generator->current());
        $generator->next();

        $this->assertFalse($generator->valid());
        $this->assertNull($generator->getReturn());
    }
}
