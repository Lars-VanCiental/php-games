<?php

namespace LVC\PHPGamesTest\Application\Player;

use LVC\PHPGames\Application\Player\SequencedBot;
use LVC\PHPGamesTest\Domain\CommandMock;
use PHPUnit\Framework\TestCase;

class SequencedBotTest extends TestCase
{
    use CommandMock;

    public function testPlayEmptySequence()
    {
        $sequencedBot = new SequencedBot([]);

        $generator = $sequencedBot->play();

        $this->assertFalse($generator->valid());
        $this->assertNull($generator->getReturn());
    }

    public function testPlay()
    {
        $sequence = [$this->getCommandMock('a'), $this->getCommandMock('b'), $this->getCommandMock('c')];

        $sequencedBot = new SequencedBot($sequence);

        $generator = $sequencedBot->play();

        $this->assertTrue($generator->valid());
        $this->assertEquals($sequence[0], $generator->current());
        $generator->next();

        $this->assertTrue($generator->valid());
        $this->assertEquals($sequence[1], $generator->current());
        $generator->next();

        $this->assertTrue($generator->valid());
        $this->assertEquals($sequence[2], $generator->current());
        $generator->next();

        $this->assertFalse($generator->valid());
        $this->assertNull($generator->getReturn());
    }
}
