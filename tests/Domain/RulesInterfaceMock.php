<?php

namespace LVC\PHPGamesTest\Domain;

use LVC\PHPGames\Domain\Gameplay\RulesInterface;
use LVC\PHPGames\Domain\Level\LevelStateInterface;

trait RulesInterfaceMock
{
    private function getRulesMock(array $levelStates, ?LevelStateInterface $finalLevelState = null): RulesInterface
    {
        $rulesMock = $this->createMock(RulesInterface::class);
        $rulesMock->method('play')->willReturnCallback(
            function () use ($levelStates, $finalLevelState) {
                yield from $levelStates;

                return $finalLevelState;
            }
        );

        return $rulesMock;
    }
}
