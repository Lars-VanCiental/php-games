<?php

namespace LVC\PHPGamesTest\Domain;

use LVC\PHPGames\Domain\Level\LevelStateInterface;

trait LevelStateInterfaceMock
{
    private function getLevelStateMock(string $state): LevelStateInterface
    {
        $levelStateMock = $this->createMock(LevelStateInterface::class);
        $levelStateMock->method('getState')->willReturn($state);

        return $levelStateMock;
    }
}
