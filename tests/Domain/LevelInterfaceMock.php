<?php

namespace LVC\PHPGamesTest\Domain;

use LVC\PHPGames\Domain\Level\LevelInterface;
use LVC\PHPGames\Domain\Level\LevelStateInterface;

trait LevelInterfaceMock
{
    private function getLevelMock(LevelStateInterface $levelState): LevelInterface
    {
        $levelMock = $this->createMock(LevelInterface::class);
        $levelMock->method('start')->willReturn($levelState);

        return $levelMock;
    }
}
