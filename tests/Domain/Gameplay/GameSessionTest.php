<?php

namespace LVC\PHPGamesTest\Domain\Gameplay;

use LVC\PHPGames\Domain\Gameplay\GameSession;
use LVC\PHPGames\Domain\Gameplay\RulesInterface;
use LVC\PHPGames\Domain\Level\LevelInterface;
use LVC\PHPGames\Domain\Level\LevelStateInterface;
use LVC\PHPGames\Domain\Player\PlayerInterface;
use LVC\PHPGamesTest\Domain\CommandMock;
use LVC\PHPGamesTest\Domain\LevelInterfaceMock;
use LVC\PHPGamesTest\Domain\LevelStateInterfaceMock;
use LVC\PHPGamesTest\Domain\PlayerInterfaceMock;
use LVC\PHPGamesTest\Domain\RulesInterfaceMock;
use PHPUnit\Framework\TestCase;

class GameSessionTest extends TestCase
{
    use LevelStateInterfaceMock;
    use RulesInterfaceMock;
    use LevelInterfaceMock;
    use PlayerInterfaceMock;
    use CommandMock;

    /** @dataProvider getPlayTestCases */
    public function testPlay(RulesInterface $rules, LevelInterface $level, PlayerInterface $player, int $expectedNumberOfStates)
    {
        $gameSession = new GameSession($rules, $level, $player);

        $iteratorCounter = 0;
        foreach ($gameSession->play() as $levelState) {
            $this->assertInstanceOf(LevelStateInterface::class, $levelState);
            $iteratorCounter++;
        }
        $this->assertEquals($expectedNumberOfStates, $iteratorCounter);
    }

    public function getPlayTestCases(): \Generator
    {
        yield 'level-state-generator-is-empty' => [
            $this->getRulesMock([]),
            $this->getLevelMock($this->getLevelStateMock('state0')),
            $this->getPlayerMock(
                [
                    $this->getCommandMock('cmd1'),
                    $this->getCommandMock('cmd2'),
                    $this->getCommandMock('cmd3'),
                    $this->getCommandMock('cmd4'),
                ]
            ),
            1,
        ];

        yield 'level-state-generator-is-empty-with-final-state' => [
            $this->getRulesMock([], $this->getLevelStateMock('statex')),
            $this->getLevelMock($this->getLevelStateMock('state0')),
            $this->getPlayerMock(
                [
                    $this->getCommandMock('cmd1'),
                    $this->getCommandMock('cmd2'),
                    $this->getCommandMock('cmd3'),
                    $this->getCommandMock('cmd4'),
                ]
            ),
            2,
        ];

        yield 'player-command-generator-is-empty' => [
            $this->getRulesMock(
                [
                    $this->getLevelStateMock('state0'),
                    $this->getLevelStateMock('state1'),
                    $this->getLevelStateMock('state2'),
                    $this->getLevelStateMock('state3'),
                ]
            ),
            $this->getLevelMock($this->getLevelStateMock('state0')),
            $this->getPlayerMock([]),
            1,
        ];

        yield 'level-state-generator-stops' => [
            $this->getRulesMock(
                [
                    $this->getLevelStateMock('state0'),
                    $this->getLevelStateMock('state1'),
                ]
            ),
            $this->getLevelMock($this->getLevelStateMock('state0')),
            $this->getPlayerMock(
                [
                    $this->getCommandMock('cmd1'),
                    $this->getCommandMock('cmd2'),
                    $this->getCommandMock('cmd3'),
                    $this->getCommandMock('cmd4'),
                ]
            ),
            2,
        ];

        yield 'level-state-generator-stops-with-final-state' => [
            $this->getRulesMock(
                [
                    $this->getLevelStateMock('state0'),
                    $this->getLevelStateMock('state1'),
                ],
                $this->getLevelStateMock('statex')
            ),
            $this->getLevelMock($this->getLevelStateMock('state0')),
            $this->getPlayerMock(
                [
                    $this->getCommandMock('cmd1'),
                    $this->getCommandMock('cmd2'),
                    $this->getCommandMock('cmd3'),
                    $this->getCommandMock('cmd4'),
                ]
            ),
            3,
        ];

        yield 'player-command-generator-stops' => [
            $this->getRulesMock(
                [
                    $this->getLevelStateMock('state0'),
                    $this->getLevelStateMock('state1'),
                    $this->getLevelStateMock('state2'),
                    $this->getLevelStateMock('state3'),
                ]
            ),
            $this->getLevelMock($this->getLevelStateMock('state0')),
            $this->getPlayerMock([$this->getCommandMock('cmd1'), $this->getCommandMock('cmd2')]),
            3,
        ];
    }
}
