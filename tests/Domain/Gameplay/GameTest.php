<?php

namespace LVC\PHPGamesTest\Domain\Gameplay;

use LVC\PHPGames\Domain\Gameplay\Game;
use LVC\PHPGames\Domain\Gameplay\GameSession;
use LVC\PHPGames\Domain\Gameplay\RulesInterface;
use LVC\PHPGames\Domain\Level\LevelFactoryInterface;
use LVC\PHPGames\Domain\Player\PlayerInterface;
use LVC\PHPGamesTest\Domain\LevelFactoryInterfaceMock;
use LVC\PHPGamesTest\Domain\LevelInterfaceMock;
use LVC\PHPGamesTest\Domain\LevelStateInterfaceMock;
use LVC\PHPGamesTest\Domain\PlayerInterfaceMock;
use LVC\PHPGamesTest\Domain\RulesInterfaceMock;
use PHPUnit\Framework\TestCase;

class GameTest extends TestCase
{
    use LevelStateInterfaceMock;
    use RulesInterfaceMock;
    use LevelInterfaceMock;
    use LevelFactoryInterfaceMock;
    use PlayerInterfaceMock;

    /** @dataProvider getStartSessionTestCases
     */
    public function testStartSession(RulesInterface $rules, LevelFactoryInterface $levelFactory, PlayerInterface $player)
    {
        $game = new Game($rules, $levelFactory);

        $gameSession = $game->startSession($player, 'test');
        $this->assertInstanceOf(GameSession::class, $gameSession);
    }

    public function getStartSessionTestCases(): \Generator
    {
        yield 'start-session' => [
            $this->getRulesMock([]),
            $this->getLevelFactoryMock($this->getLevelMock($this->getLevelStateMock('state0'))),
            $this->getPlayerMock([]),
        ];
    }
}
