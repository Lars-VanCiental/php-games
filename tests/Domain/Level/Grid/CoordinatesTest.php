<?php

namespace LVC\PHPGamesTest\Domain\Level\Grid;

use LVC\PHPGames\Domain\Level\Grid\Coordinates;
use PHPUnit\Framework\TestCase;

class CoordinatesTest extends TestCase
{
    /** @dataProvider getWithinRangeTestCases */
    public function testWithinRange(Coordinates $coordinates, int $width, int $height)
    {
        $this->assertTrue($coordinates->checkWithin($width, $height));
    }

    public function getWithinRangeTestCases(): \Generator
    {
        yield 'middle' => [
            new Coordinates(5, 5),
            10,
            10,
        ];

        yield  'start-x' => [
            new Coordinates(0, 5),
            10,
            10,
        ];

        yield  'end-x' => [
            new Coordinates(9, 5),
            10,
            10,
        ];

        yield  'start-y' => [
            new Coordinates(5, 0),
            10,
            10,
        ];

        yield  'end-y' => [
            new Coordinates(5, 9),
            10,
            10,
        ];
    }

    /** @dataProvider getOutOfRangeTestCases */
    public function testOutOfRange(Coordinates $coordinates, int $width, int $height)
    {
        $this->assertFalse($coordinates->checkWithin($width, $height));
    }

    public function getOutOfRangeTestCases(): \Generator
    {
        yield  'before-x' => [
            new Coordinates(-1, 5),
            10,
            10,
        ];

        yield  'after-x' => [
            new Coordinates(10, 5),
            10,
            10,
        ];

        yield  'before-y' => [
            new Coordinates(5, -1),
            10,
            10,
        ];

        yield  'after-y' => [
            new Coordinates(5, 10),
            10,
            10,
        ];

        yield  'before-both' => [
            new Coordinates(-1, -1),
            10,
            10,
        ];

        yield  'after-both' => [
            new Coordinates(10, 10),
            10,
            10,
        ];
    }
}
