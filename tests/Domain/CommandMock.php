<?php

namespace LVC\PHPGamesTest\Domain;

use LVC\PHPGames\Domain\Gameplay\Command;

trait CommandMock
{
    private function getCommandMock(string $command): Command
    {
        $levelStateMock = $this->createMock(Command::class);
        $levelStateMock->method('getCommand')->willReturn($command);

        return $levelStateMock;
    }
}
