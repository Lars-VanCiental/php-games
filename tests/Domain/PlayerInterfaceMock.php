<?php

namespace LVC\PHPGamesTest\Domain;

use LVC\PHPGames\Domain\Player\PlayerInterface;

trait PlayerInterfaceMock
{
    private function getPlayerMock(array $commands): PlayerInterface
    {
        $levelStateMock = $this->createMock(PlayerInterface::class);
        $levelStateMock->method('play')->willReturnCallback(
            function () use ($commands): \Generator {
                yield from $commands;
            }
        );

        return $levelStateMock;
    }
}
