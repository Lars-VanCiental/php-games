<?php

namespace LVC\PHPGamesTest\Domain;

use LVC\PHPGames\Domain\Level\LevelFactoryInterface;
use LVC\PHPGames\Domain\Level\LevelInterface;

trait LevelFactoryInterfaceMock
{
    private function getLevelFactoryMock(LevelInterface $level): LevelFactoryInterface
    {
        $levelFactoryMock = $this->createMock(LevelFactoryInterface::class);
        $levelFactoryMock->method('loadLevel')->willReturn($level);

        return $levelFactoryMock;
    }
}
