<?php

require __DIR__.'/../vendor/autoload.php';

use LVC\PHPGames\Architecture\Commands\Games\PlaySokobanCommand;
use Symfony\Component\Console\Application;

$application = new Application();

$application->add(new PlaySokobanCommand());

$application->run();